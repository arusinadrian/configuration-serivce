import { AppConfigurationClient, GetConfigurationSettingResponse, parseSecretReference } from '@azure/app-configuration';
import { configPathsFile } from './index';
import { parseKeyVaultSecretIdentifier, SecretClient } from "@azure/keyvault-secrets";
import { DefaultAzureCredential } from "@azure/identity";


export interface IConfigPathsFile {
    [key: string]: {
        appConfigurationKey: string;
        localValue: string;
        labelOverride?: string; // possible values none, dev, tst, null
        isSecret?: boolean;
    }
}

export interface ILoadConfig {
    [key: string]: string;
}

export class Config {
    client: AppConfigurationClient | undefined;
    environment = 'local';
    configPathsFile: IConfigPathsFile;

    constructor(environment: string, configPathsFile: IConfigPathsFile, connectionString?: string) {
        this.environment = environment;
        this.configPathsFile = configPathsFile;

        if (environment && this.environment !== 'local' && connectionString) 
            this.client = new AppConfigurationClient(connectionString);
    }

    get isLocal() {
        return this.environment === 'local';
    }

    get serviceUrl() {
        return process.env.SERVICE_URL;
    }    

    private async getSecret(setting: GetConfigurationSettingResponse, key: string, label: string) {
        const parsedSecretReference = parseSecretReference(setting);
        const { name: secretName, vaultUrl } = parseKeyVaultSecretIdentifier(
            parsedSecretReference.value.secretId
        );

        const secretClient = new SecretClient(vaultUrl, new DefaultAzureCredential());
        try {
            const secret = await secretClient.getSecret(secretName);

            if (!secret.value) {
                console.error('Missing secret value', { key, environment: label });

                throw new Error('Missing secret value');
            }

            return secret.value;
        } catch (error) {
            console.error('Couldn\'t fetch secret for key', { key, environment: label, error });
            throw new Error(`Couldn't fetch secret for key: ${key}`);
        }
    }

    async getConfiguration(key: string, labelOverride?: string, isSecret?: boolean): Promise<string> {
        let label = labelOverride ? labelOverride : this.environment;

        try {
            if (!this.client) throw new Error('App configuration client is not initialized.');

            let setting: GetConfigurationSettingResponse;
            
            switch(labelOverride) {
                case 'none': 
                    setting = await this.client.getConfigurationSetting({ key })
                    break;
                default: 
                    setting  = await this.client.getConfigurationSetting({ key, label });
                    break;
            }

            if (isSecret) {
                return this.getSecret(setting, key, label);
            }

            if (!setting.value) {
                console.error('Missing key value', { key, environment: label });

                throw new Error('Missing key value');
            }

            return setting.value;
        } catch (error) {
            console.error('Failed to load key', { key, environment: label, error });
    
            throw new Error(`Failed to load key ${key}`);
        }
    }

    async loadConfig<T extends ILoadConfig>(): Promise<T> {
        const configObject = Object.keys(configPathsFile).reduce(async (promisedAcc, curr) => { 
            let acc = await promisedAcc;
            
            if (this.isLocal) {
                const value = configPathsFile[curr].localValue;
                acc[curr] = value; 
                return acc;
            }

            const value = await this.getConfiguration(configPathsFile[curr].appConfigurationKey, configPathsFile[curr].labelOverride, configPathsFile[curr].isSecret);

            acc[curr] = value; 

            return acc;

        }, Promise.resolve(({} as ILoadConfig)));

        return configObject as Promise<T>;
    }
}