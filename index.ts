import 'dotenv/config';
import { Config, IConfigPathsFile, ILoadConfig } from './config';

const environment = process.env.NODE_ENV ?? 'local';
const connectionString = process.env.CONNECTION_STRING ?? '';

interface IClickFlowConfig extends ILoadConfig {
    apimClickFinalizeRouteUrl: string;
    apimHost: string
}

export const configPathsFile: IConfigPathsFile = {
    apimClickFinalizeRouteUrl: {
        appConfigurationKey: 'SERVICE_URL',
        localValue: process.env.SERVICE_URL ?? '',
        isSecret: true,
    },
    apimHost: {
        appConfigurationKey: 'SERVICE_NAME',
        localValue: process.env.SERVICE_NAME ?? '',
        labelOverride: 'development'
    }
}

const config = new Config(environment, configPathsFile, connectionString);

const loadedConfig = config.loadConfig<IClickFlowConfig>();

Promise.resolve(loadedConfig).then(result => console.log(result));
